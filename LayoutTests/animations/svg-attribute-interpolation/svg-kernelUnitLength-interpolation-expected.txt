CONSOLE WARNING: SVG's SMIL animations (<animate>, <set>, etc.) are deprecated and will be removed. Please use CSS animations or Web animations instead.

SVG SMIL:
PASS: kernelUnitLength from [1] to [6 11] was [1, 1] at 0
PASS: kernelUnitLength from [1] to [6 11] was [2, 3] at 0.2
PASS: kernelUnitLength from [1] to [6 11] was [4, 7] at 0.6
PASS: kernelUnitLength from [1] to [6 11] was [6, 11] at 1
PASS: kernelUnitLength from [-2 10] to [3 10] was [-2 10] at 0
PASS: kernelUnitLength from [-2 10] to [3 10] was [-1, 10] at 0.2
PASS: kernelUnitLength from [-2 10] to [3 10] was [1, 10] at 0.6
PASS: kernelUnitLength from [-2 10] to [3 10] was [3, 10] at 1

Web Animations API:
PASS: kernelUnitLength from [1] to [6 11] was [-1, -3] at -0.4
PASS: kernelUnitLength from [1] to [6 11] was [1, 1] at 0
PASS: kernelUnitLength from [1] to [6 11] was [2, 3] at 0.2
PASS: kernelUnitLength from [1] to [6 11] was [4, 7] at 0.6
PASS: kernelUnitLength from [1] to [6 11] was [6, 11] at 1
PASS: kernelUnitLength from [1] to [6 11] was [8, 15] at 1.4
PASS: kernelUnitLength from [-2 10] to [3 10] was [-4, 10] at -0.4
PASS: kernelUnitLength from [-2 10] to [3 10] was [-2 10] at 0
PASS: kernelUnitLength from [-2 10] to [3 10] was [-1, 10] at 0.2
PASS: kernelUnitLength from [-2 10] to [3 10] was [1, 10] at 0.6
PASS: kernelUnitLength from [-2 10] to [3 10] was [3, 10] at 1
PASS: kernelUnitLength from [-2 10] to [3 10] was [5, 10] at 1.4

