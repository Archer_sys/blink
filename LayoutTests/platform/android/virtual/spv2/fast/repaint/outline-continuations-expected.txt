{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "repaintRects": [
        [18, 170, 200, 20],
        [18, 150, 200, 20],
        [18, 130, 200, 20],
        [18, 110, 200, 20],
        [16, 168, 204, 24],
        [16, 148, 204, 44],
        [16, 148, 202, 23],
        [16, 128, 204, 64],
        [16, 108, 204, 84],
        [16, 108, 204, 84]
      ],
      "paintInvalidationClients": [
        "InlineTextBox 'CONTENTS'",
        "RootInlineBox",
        "InlineTextBox 'CONTENTS'",
        "RootInlineBox",
        "LayoutBlockFlow (anonymous)",
        "LayoutInline SPAN id='outer'",
        "InlineFlowBox",
        "LayoutBlockFlow (anonymous)",
        "LayoutBlockFlow (anonymous)",
        "LayoutInline SPAN id='outer'",
        "InlineFlowBox",
        "LayoutBlockFlow (anonymous)",
        "LayoutBlockFlow (anonymous)",
        "LayoutInline SPAN id='outer'"
      ]
    }
  ]
}

