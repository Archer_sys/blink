layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x320
  LayoutBlockFlow {HTML} at (0,0) size 800x320
    LayoutBlockFlow {BODY} at (8,16) size 784x296
      LayoutBlockFlow {P} at (0,0) size 784x18
        LayoutText {#text} at (0,0) size 61x18
          text run at (0,0) width 61: "Tests for "
        LayoutInline {A} at (0,0) size 311x18 [color=#0000EE]
          LayoutText {#text} at (60,0) size 311x18
            text run at (60,0) width 311: "https://bugs.webkit.org/show_bug.cgi?id=24552"
      LayoutBlockFlow (anonymous) at (0,34) size 784x124
        LayoutText {#text} at (120,106) size 4x18
          text run at (120,106) width 4: " "
        LayoutInline {A} at (0,0) size 62x18 [color=#0000EE]
          LayoutText {#text} at (124,106) size 62x18
            text run at (124,106) width 62: "Link here"
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {DIV} at (0,278) size 784x18
        LayoutText {#text} at (0,0) size 170x18
          text run at (0,0) width 170: "Found link node, so PASS"
layer at (18,60) size 100x100
  LayoutBlockFlow {DIV} at (10,10) size 100x100 [bgcolor=#808080]
